diff --git a/src/Proto3/Wire/Reverse/Prim.hs b/src/Proto3/Wire/Reverse/Prim.hs
index 3113baa..31deb68 100644
--- a/src/Proto3/Wire/Reverse/Prim.hs
+++ b/src/Proto3/Wire/Reverse/Prim.hs
@@ -116,6 +116,10 @@ import           GHC.Exts                      ( Addr#, Int#, Proxy#,
                                                  and#, inline, or#,
                                                  plusAddr#, plusWord#, proxy#,
                                                  uncheckedShiftRL# )
+#if MIN_VERSION_base(4,16,0)
+import           GHC.Exts                      ( Word8#, Word32#,
+                                                 word32ToWord#, wordToWord8# )
+#endif
 import           GHC.IO                        ( IO(..) )
 import           GHC.Int                       ( Int(..) )
 import           GHC.Ptr                       ( Ptr(..) )
@@ -663,7 +667,7 @@ charUtf8 = \ch -> case fromIntegral (ord ch) of W# x -> wordUtf8 x
       Word# ->
       FixedPrim (n + 1)
     lsb = \p x -> p (uncheckedShiftRL# x 6#) &<>
-                  word8 (W8# (plusWord# 0x80## (and# x 0x3F##)))
+                  word8 (W8# (wordToWord8Compat# (plusWord# 0x80## (and# x 0x3F##))))
     {-# INLINE lsb #-}
 
     p1 :: Word# -> FixedPrim 1
@@ -671,10 +675,10 @@ charUtf8 = \ch -> case fromIntegral (ord ch) of W# x -> wordUtf8 x
     p3 :: Word# -> FixedPrim 3
     p4 :: Word# -> FixedPrim 4
 
-    p1 x = word8 (W8# x)
-    p2 = lsb (\x -> word8 (W8# (plusWord# 0xC0## x)))
-    p3 = lsb (lsb (\x -> word8 (W8# (plusWord# 0xE0## x))))
-    p4 = lsb (lsb (lsb (\x -> word8 (W8# (plusWord# 0xF0## x)))))
+    p1 x = word8 (W8# (wordToWord8Compat# x))
+    p2 = lsb (\x -> word8 (W8# (wordToWord8Compat# (plusWord# 0xC0## x))))
+    p3 = lsb (lsb (\x -> word8 (W8# (wordToWord8Compat# (plusWord# 0xE0## x)))))
+    p4 = lsb (lsb (lsb (\x -> word8 (W8# (wordToWord8Compat# (plusWord# 0xF0## x))))))
 
     {-# INLINE p1 #-}
     {-# INLINE p2 #-}
@@ -719,7 +723,7 @@ word32Base128LEVar_inline = \(W32# x0) ->
     wordBase128LEVar_choose 3 wordBase128LE_p3 $
     wordBase128LEVar_choose 4 wordBase128LE_p4 $
     (\x -> liftFixedPrim (wordBase128LE_p5 0## x))
-  ) x0
+  ) (word32ToWordCompat# x0)
 {-# INLINE word32Base128LEVar_inline #-}
 
 wordBase128LEVar_choose ::
@@ -742,13 +746,13 @@ wordBase128LE_msb ::
   (Word# -> Word# -> FixedPrim n) ->
   Word# -> Word# -> FixedPrim (n + 1)
 wordBase128LE_msb = \p m x ->
-    p 0x80## x &<> word8 (W8# (or# m (uncheckedShiftRL# x s)))
+    p 0x80## x &<> word8 (W8# (wordToWord8Compat# (or# m (uncheckedShiftRL# x s))))
   where
     !(I# s) = 7 * fromInteger (natVal' (proxy# :: Proxy# n))
 {-# INLINE wordBase128LE_msb #-}
 
 wordBase128LE_p1 :: Word# -> Word# -> FixedPrim 1
-wordBase128LE_p1 = \m x -> word8 (W8# (or# m x))
+wordBase128LE_p1 = \m x -> word8 (W8# (wordToWord8Compat# (or# m x)))
 {-# INLINE wordBase128LE_p1 #-}
 
 wordBase128LE_p2 :: Word# -> Word# -> FixedPrim 2
@@ -813,10 +817,10 @@ word64Base128LEVar_big x = pif (W64# x <= shiftL 1 60 - 1) p60 p64
           word32Base128LEVar (W32# (shR 28))
 
     p64 = ( liftFixedPrim (word28Base128LE x32) &<>
-            liftFixedPrim (word28Base128LE (shR 28)) ) &<>
-          word14Base128LEVar (shR 56)
+            liftFixedPrim (word28Base128LE (word32ToWordCompat# (shR 28))) ) &<>
+          word14Base128LEVar (word32ToWordCompat# (shR 56))
 
-    x32 = case fromIntegral (W64# x) of W32# y -> y
+    x32 = case fromIntegral (W64# x) of W32# y -> word32ToWordCompat# y
 
     shR s = case fromIntegral (shiftR (W64# x) s) of W32# y -> y
 {-# NOINLINE word64Base128LEVar_big #-}
@@ -836,3 +840,17 @@ vectorFixedPrim f = etaBuildR $ \v ->
   where
     w = fromInteger (natVal' (proxy# :: Proxy# w))
 {-# INLINE vectorFixedPrim #-}
+
+#if MIN_VERSION_base(4,16,0)
+word32ToWordCompat# :: Word32# -> Word#
+word32ToWordCompat# = word32ToWord#
+
+wordToWord8Compat# :: Word# -> Word8#
+wordToWord8Compat# = wordToWord8#
+#else
+word32ToWordCompat# :: Word# -> Word#
+word32ToWordCompat# x = x
+
+wordToWord8Compat# :: Word# -> Word#
+wordToWord8Compat# x = x
+#endif
