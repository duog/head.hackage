diff --git a/primitive-sort.cabal b/primitive-sort.cabal
index 0aa87d5..1328a2d 100644
--- a/primitive-sort.cabal
+++ b/primitive-sort.cabal
@@ -1,6 +1,7 @@
 cabal-version: 2.0
 name: primitive-sort
 version: 0.1.0.0
+x-revision: 4
 synopsis: Sort primitive arrays
 description:
   This library provides a stable sorting algorithm for primitive arrays.
@@ -27,7 +28,7 @@ library
       base >= 0.4.9 && < 5
     , primitive >= 0.6.4.0
     , ghc-prim
-    , contiguous >= 0.1 && < 0.2
+    , contiguous >= 0.2 && < 0.6
   ghc-options: -O2
   default-language: Haskell2010
 
diff --git a/src/Data/Primitive/Sort.hs b/src/Data/Primitive/Sort.hs
index 83a141c..beb5af9 100644
--- a/src/Data/Primitive/Sort.hs
+++ b/src/Data/Primitive/Sort.hs
@@ -29,7 +29,7 @@ import GHC.ST (ST(..))
 import GHC.IO (IO(..))
 import GHC.Int (Int(..))
 import Control.Monad
-import GHC.Prim
+import GHC.Exts
 import Control.Concurrent (getNumCapabilities)
 import Data.Primitive.Contiguous (Contiguous,Mutable,Element)
 import qualified Data.Primitive.Contiguous as C
@@ -116,7 +116,7 @@ sortMutable !dst = do
     then insertionSortRange dst 0 len
     else do
       work <- C.new len
-      C.copyMutable work 0 dst 0 len 
+      C.copyMutable work 0 dst 0 len
       caps <- unsafeEmbedIO getNumCapabilities
       let minElemsPerThread = 20000
           maxThreads = unsafeQuot len minElemsPerThread
@@ -182,8 +182,8 @@ sortTaggedMutableN !len !dst !dstTags = if len < thresholdTagged
     insertionSortTaggedRange dst dstTags 0 len
     return (dst,dstTags)
   else do
-    work <- C.cloneMutable dst 0 len 
-    workTags <- C.cloneMutable dstTags 0 len 
+    work <- C.cloneMutable dst 0 len
+    workTags <- C.cloneMutable dstTags 0 len
     caps <- unsafeEmbedIO getNumCapabilities
     let minElemsPerThread = 20000
         maxThreads = unsafeQuot len minElemsPerThread
@@ -210,7 +210,7 @@ sortUnique src = runST $ do
 -- | Sort an immutable array. Only a single copy of each duplicated
 -- element is preserved. This operation may run in-place, or it may
 -- need to allocate a new array, so the argument may not be reused
--- after this function is applied to it. 
+-- after this function is applied to it.
 sortUniqueMutable :: (Contiguous arr, Element arr a, Ord a)
   => Mutable arr s a
   -> ST s (Mutable arr s a)
@@ -317,7 +317,7 @@ splitMergeParallel !arr !work !level !start !end = if level > 1
     else do
       let !mid = unsafeQuot (end + start) 2
           !levelDown = half level
-      tandem 
+      tandem
         (splitMergeParallel work arr levelDown start mid)
         (splitMergeParallel work arr levelDown mid end)
       mergeParallel work arr level start mid end
@@ -337,7 +337,7 @@ splitMergeParallelTagged !arr !work !arrTags !workTags !level !start !end = if l
   then do
     let !mid = unsafeQuot (end + start) 2
         !levelDown = half level
-    tandem 
+    tandem
       (splitMergeParallelTagged work arr workTags arrTags levelDown start mid)
       (splitMergeParallelTagged work arr workTags arrTags levelDown mid end)
     mergeParallelTagged work arr workTags arrTags level start mid end
@@ -395,7 +395,7 @@ mergeParallel !src !dst !threads !start !mid !end = do
          -> Int -- previous B end
          -> Int -- how many chunk have we already iterated over
          -> ST s Int
-      go !prevEndA !prevEndB !ix = 
+      go !prevEndA !prevEndB !ix =
         if | prevEndA == mid && prevEndB == end -> return ix
            | prevEndA == mid -> do
                forkST_ $ do
@@ -440,7 +440,7 @@ mergeParallel !src !dst !threads !start !mid !end = do
                  mergeNonContiguous src dst startA endA startB endB startDst
                  putLock lock
                go endA endB (ix + 1)
-  !endElem <- C.read src (start + chunk) 
+  !endElem <- C.read src (start + chunk)
   !endA <- findIndexOfGtElem src (endElem :: a) start mid
   !endB <- findIndexOfGtElem src endElem mid end
   forkST_ $ do
@@ -474,7 +474,7 @@ mergeParallelTagged !src !dst !srcTags !dstTags !threads !start !mid !end = do
          -> Int -- previous B end
          -> Int -- how many chunk have we already iterated over
          -> ST s Int
-      go !prevEndA !prevEndB !ix = 
+      go !prevEndA !prevEndB !ix =
         if | prevEndA == mid && prevEndB == end -> return ix
            | prevEndA == mid -> do
                forkST_ $ do
@@ -519,7 +519,7 @@ mergeParallelTagged !src !dst !srcTags !dstTags !threads !start !mid !end = do
                  mergeNonContiguousTagged src dst srcTags dstTags startA endA startB endB startDst
                  putLock lock
                go endA endB (ix + 1)
-  !endElem <- C.read src (start + chunk) 
+  !endElem <- C.read src (start + chunk)
   !endA <- findIndexOfGtElem src (endElem :: k) start mid
   !endB <- findIndexOfGtElem src endElem mid end
   forkST_ $ do
@@ -685,7 +685,7 @@ insertionSortRange !arr !start !end = go start
       insertElement arr (a :: a) start ix
       go (ix + 1)
     else return ()
-    
+
 insertElement :: forall arr s a. (Contiguous arr, Element arr a, Ord a)
   => Mutable arr s a
   -> a
@@ -725,7 +725,7 @@ insertionSortTaggedRange !karr !varr !start !end = go start
       insertElementTagged karr varr a v start ix
       go (ix + 1)
     else return ()
-    
+
 insertElementTagged :: forall karr varr s k v. (Contiguous karr, Element karr k, Ord k, Contiguous varr, Element varr v)
   => Mutable karr s k
   -> Mutable varr s v
